import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
from bs4 import BeautifulSoup
import spacy
import matplotlib.pyplot as plt

#fiserul csv cu datele de antrenare si test
filename = 'stackoverflow_data.csv'

#utility functions
def remove_html(text):
    return BeautifulSoup(text, features="html.parser").get_text()

def remove_digits(text):
    filtered_text = ''.join([letter for letter in text if not letter.isdigit()])
    return filtered_text

def select_post(post_number, doc):
    doc_scores = tf_idf_matrix[post_number]

    #verifica daca pot fi alese top 3 cuvinte (daca propozitia e mai lunga)
    length = len(doc[post_number].split())
    if length < 3:
        print("Warning: the post contains less than 3 relevant words")
        print("The post contains " + str(length) + ' relevant words')

    df_tf_idf = pd.DataFrame(doc_scores, index=tf_idf_vectorizer.get_feature_names(), columns=['tf_idf_scores'])
    df_tf_idf.sort_values(by=['tf_idf_scores'], inplace=True, ascending=False) #sorteaza descrescator dupa importanta cuvantului in propozitie

    top_values = list(df_tf_idf.axes[0].values[0:3])
    print("The top 3 topics are: ")
    print(top_values)

def plot_comments_time(plot_data):

    plot_data.hist('time', bins=24, edgecolor='black', weights=plot_data['view_count']) #24 bins -> 24 hours
    plt.title('Common creation times for popular posts')
    plt.xlabel("Time of day")
    plt.ylabel('Total number of views for posts created in time interval')
    plt.show(block=False) #keep executing code after plotting

def get_data(file):


    nlp = spacy.load('en_core_web_sm')
    df = pd.read_csv(file, delimiter=',')


    #store view count for each post
    view_counts = df['view_count'].fillna(0).astype(int)  #fill missing elements with 0 and convert to int

    #store creation hour for each post
    df['DateTime'] = pd.to_datetime(df['creation_date'], format = "%Y-%m-%d %H:%M:%S.%f %Z", errors='coerce')  # datele care nu respecta formatul standard vor fi trecute ca nan
    hours = [d.hour for d in df['DateTime']]

    #create dataframe for histogram
    plot_data = {'view_count': view_counts,
                'time': hours
                }
    plot_dataframe = pd.DataFrame(plot_data, columns=['view_count', 'time']).dropna()   #  renunta la liniile care contin nan, sunt putine si nu au impact mare asupra rezultatului final



    #formeaza o singura propozitie compusa din titlu si post pentru a extrage cuvintele relevante
    title = df['title'].fillna(' ').values  #multe post-uri au titlul lipsa asa ca nan-ul va fi inlocuit cu un spatiu
    body = df['body'].fillna(' ').values
    relevant_text = title + ' ' + body

    #vectorize functions to apply throughout numpy array
    remove_html_vec = np.vectorize(remove_html)
    remove_digits_vec = np.vectorize(remove_digits)

    #eliminia elementele de html si pe cele numerice, irelevante pentru extragerea top 3 topicuri
    relevant_text = remove_html_vec(relevant_text)
    relevant_text = remove_digits_vec(relevant_text)

    #applying lemmatizer and word tokenizer
    for index in range(len(relevant_text)):
        doc = nlp(str(relevant_text[index]))
        lemmatized_tokens = [token.lemma_ for token in doc if token.lemma_ != "-PRON-"]  #filtrare de pronume, intalnite foarte des

        relevant_text[index] = ' '
        relevant_text[index] = relevant_text[index].join(lemmatized_tokens)     #reconstructia propozitiei dupa ce textul a fost curatat

    return relevant_text, plot_dataframe


doc, plot_data = get_data(filename)


# creaza matricea cu scoruri tf-idf
tf_idf_vectorizer = TfidfVectorizer(lowercase=True, stop_words='english')
tf_idf_vectorizer.fit(doc)
tf_idf_matrix = tf_idf_vectorizer.transform(doc).toarray()


# select the index of the post in the csv file to be analyzed
select_post(20, doc)

#plot graph with popular time of day for popular posts
plot_comments_time(plot_data)


